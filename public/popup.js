$('document').ready(function () {
    $('.jm9k').on('click', function (e) {
        e.preventDefault();
        let href = $(this).attr('href');
        getContent(href, true);
    });

    $('.close').click(function () {
        $('.pop_up').fadeOut();
        $('main').css('filter', 'none');
        window.history.back();
    });
});

window.addEventListener("popstate", function (e) {
    getContent(location.pathname, false);
});

function getContent(url, addEntry) {
    if (addEntry === true) {
        $('.pop_up').fadeIn();
        $('main').css('filter', 'blur(5px)');
        window.history.pushState({ page: 1 }, null, '?#form_popup');
    }
    else {
        $('.pop_up').fadeOut();
        $('main').css('filter', 'none');
        window.history.replaceState({ page: 0 }, null, 'https://goldenfoxx0810.gitlab.io/web9/');
    }
}
